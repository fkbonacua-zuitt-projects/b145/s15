console.log("Hello World");
let givenName = 'John';
let familyName = 'Smith';
let ageStats = 'is 30 years of age';

function fullName(){
	console.log(givenName + ' ' + familyName + ' ' + ageStats);
}
fullName();

function returnString(){
	return 'This was printed inside of the function';
}
let functionOutput = returnString();
console.log(functionOutput);

let value1 = 'The value of';
let value2 = 'true';
let value3 = 'false';

function marriageValue(value1, value2){
	console.log(value1 + ' is Married:' + value2);
}
marriageValue(value1, value2);