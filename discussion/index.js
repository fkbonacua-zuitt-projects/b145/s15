// lets include a confirm message
// console.log() is used to output/display messages in the console of your browser.
console.log('Hello From JS');
// THERE are multiple ways toi display messages in the console using different formats.

// error message
console.error('Error something');

// warning message
console.warn('Ooops Warning');

// syntax:
	// function nameOfFunction() {
	// 	//instruction/procedures
	// 	return
	// }

// lets create a function that will display an error
function errorMessage(){
	console.error('Error oh No!!!');
}

errorMessage(); //callout the function

function greetings() { //DECLARATION
	// procedures
	console.log('Salutations from JS!');
}

// INVOCATION
greetings();

// There are different methods in declaring a function in JS

// [SECTION 1:] Variables declared outside a function can be used INSIDE a function.

let givenName = 'John';
let familyName = 'Smith';

// lets create a function that will utilize the information outside it's scope.
function fullName(){
		// combine the values of the info outside and display it inside the console.
		console.log(givenName + ' ' + familyName);
}

// Invoking/Calling out functions we do it, by adding parenthesis() after the function name.
fullName();

// [SECTION 2:] "blocked scope" variables, this means that variables can only be used within the scope of the function.

// lets create a function that will allow us to compute the values of 2 variables.
function computeTotal(){
		let numA = 20;
		let numB = 5;
		// add the values and display it inside the console.
		console.log(numA + numB);
}

// console.log(numA); //the variable cannot be used outside the scope of the function where it was declared.
// call out the function
computeTotal();

// [SECTION 3:] Functions with Parameters

// "Parameter" -> acts as a variable or a container/catchers that only exists inside a function. A parameter is used to store information that is provided to a function when it is called or invoked.

// lets create a function that emulate a pokemon battle.
function pokemon(pangalan) {
		// were going to use the "parameter" declared on this function to be processed and displayed inside the console.
		console.log('I Choose you: ' + pangalan);
}

// invocation
pokemon('Pikachu');

// parameters VS arguments?

// Argument -> is the "ACTUAL" value that is provided inside a function for it to work properly. The TERM argument is used when functions are called/invoked as compared to paremeters when a function is simply declared.

// [SECTION 4:] Functions with Multiple parameters

// lets declare a function that will get the sum of multiple values.

function addNumbers(numA, numB, numC, numD, numE){
		// display the sum inside the console.
		console.log(numA + numB + numC + numD + numE);
}

// NaN: Not a Number
addNumbers(1,2,3,4,5); //15

// lets create a function that will generate a person's full Name
function createFullName(fName, mName, lName){
		console.log(lName + ', ' + fName + ' ' + mName);
}

// invoke the function and pass down the arguments needed that will take the place of each parameter.
createFullName('Juan', 'Dela', 'Cruz');
// upon using multiple arguments it will correspond to the number of "parameters" declared in a function in succeeding order. unless reordered inside the function.

// [SECTION 5:] Using variables as Arguments

// create a function that will display the stats of a pokemon in battle.

let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC = 'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack){
	console.log(name + ' use ' + attack);
}

pokemonStats(selectedPokemon, attackB);
// Ratata use Thunderbolt

// Note: using variables as arguments will allow us to utilize code reusability, this will help us reduce the amount of code that we have to write down.

// The use of the "return" expression/statement
// the return statement is used to display the output of a function.

// [SECTION 6:] The RETURN statement
	
	// the "return" statement allows the output of a function to be passed to the line/block of code that called the function.

// create a function that will return a string message.
function returnString(){
		return 'Hello World';
		 2 + 1; //3
}

// lets repackage the result of this function inside a new variable
let functionOutput = returnString();
console.log(functionOutput);

// lets create a simple function to demonstrate the use and behavior of the return statement again.
function dialog(){
	console.log('Ooops! I did it Again');
	console.log("Dont you know that youre toxic");
	'Im a Slave for you!';
	return console.log('Sometimes I Run!');
}
// note: any Block/line of code that will come after our "return" statement is going to be ignored because it came after the end of the function execution.
// The main purpose of the return statement is to identify which will be the final output/result of the function and at the same time determine the end of the function statement.

// invocation
console.log(dialog());

// [SECTION 7:] Using Functions as Arguments.

// => Function parameters can also accept other functions are their argument.
// =>Some complex functions use other functions as their arguments to perform more complicated/complex results.

// werejust going to create a simple function to be used as an argument later.
function argumentSaFunction(){
	console.log('This function was passed as an argument');
}

function invokeFunction(argumentNaFunction, pangalawangFunction) {
		argumentNaFunction();
		pangalawangFunction(selectedPokemon, attackB);
		pangalawangFunction(selectedPokemon, attackA);
		pangalawangFunction(selectedPokemon, attackC);
}

// invoke the function above
invokeFunction(argumentSaFunction, pokemonStats);

// IF YOU WANT to see details/information about a function
console.log(invokeFunction);